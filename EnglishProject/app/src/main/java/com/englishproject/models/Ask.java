package com.englishproject.models;

import java.util.ArrayList;

/**
 * Created by jean-baptistedominguez on 12/02/2016.
 */
public class Ask {
    Long id;
    String title;
    ArrayList<String> answers;
    String response;

    public Ask(Long id, String title, ArrayList<String> answers, String response) {
        this.id = id;
        this.title = title;
        this.answers = answers;
        this.response = response;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public String getResponse() {
        return response;
    }
}
