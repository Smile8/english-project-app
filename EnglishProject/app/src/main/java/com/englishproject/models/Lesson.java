package com.englishproject.models;

import java.util.ArrayList;

/**
 * Created by panpriape on 15/03/16.
 */
public class Lesson {
    Long id;

    String title;
    String lesson;
    ArrayList<String> examples;

    public Lesson(Long id, String title, String lesson, ArrayList<String> examples) {
        this.id = id;
        this.title = title;
        this.lesson = lesson;
        this.examples = examples;
    }

    public ArrayList<String> getExamples() {
        return examples;
    }

    public void setExamples(ArrayList<String> examples) {
        this.examples = examples;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }
}
