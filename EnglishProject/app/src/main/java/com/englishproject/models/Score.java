package com.englishproject.models;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jean-baptistedominguez on 13/03/2016.
 */
public class Score {

    private int points = 0;
    private HashMap<Ask, String> responses = new HashMap<Ask, String> ();
    private ArrayList<Ask> asks = new ArrayList<Ask>();

    public void addPoints(int points) {
        this.points += points;
    }

    public void addResponse(Ask ask, String response) {
        responses.put(ask, response);
    }

    public ArrayList<Ask> getAsks() {
        return asks;
    }

    public HashMap<Ask, String> getResponses() {
        return responses;
    }

    public String getResponseByAsk(Ask ask) {
        return responses.get(ask);
    }

    public int getPoints() {
        return points;
    }
}
