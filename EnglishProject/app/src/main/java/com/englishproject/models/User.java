package com.englishproject.models;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jean-baptistedominguez on 13/03/2016.
 */
public class User {
    private String firstname;
    private String lastname;
    private String origine;
    private String facebookId;
    private HashMap<Part, ArrayList<Score>> scores = new HashMap<Part, ArrayList<Score>>();

    public User() {
        this.lastname = "..";
        this.firstname = "..";
        this.origine = "FR";
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public HashMap<Part, ArrayList<Score>> getScores() {
        return scores;
    }

    public Score getHighScore(Part part) {
        if (scores.containsKey(part)) {
            Score scoreMax = null;
            for (Score score : scores.get(part)) {
                if (scoreMax == null) {
                    scoreMax = score;
                }
                else if (score.getPoints()>scoreMax.getPoints()) {
                    scoreMax = score;
                }
            }
            return scoreMax;
        }
        return null;
    }

    public void saveScore (Part part, Score score) {
        if (!scores.containsKey(part)) {
            scores.put(part, new ArrayList<Score>());
        }
        scores.get(part).add(score);
    }
}
