package com.englishproject.models;

/**
 * Created by jean-baptistedominguez on 03/02/2016.
 */
public class Part {
    private Long id;
    private String name;
    private int highScore;

    public Part(Long id, String name, int highScore) {
        this.id = id;
        this.name = name;
        this.highScore = highScore;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getHighScore() {
        return highScore;
    }

    @Override
    public String toString() {
        return "Part{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", highScore=" + highScore +
                '}';
    }
}
