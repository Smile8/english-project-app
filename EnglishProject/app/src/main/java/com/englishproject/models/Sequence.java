package com.englishproject.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by jean-baptistedominguez on 17/02/2016.
 */
public class Sequence {
    Long id;

    ArrayList<String> textes;
    String imageUrl;
    String soundUrl;
    Bitmap bmp;
    MediaPlayer mediaPlayer;

    ArrayList<Ask> asks;

    public Sequence(Long id, String imageUrl, String soundUrl, ArrayList<Ask> asks) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.soundUrl = soundUrl;
        this.asks = asks;
    }

    public void setBmp(Bitmap bmp) {
        this.bmp = bmp;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public Bitmap getBmp() {
        return bmp;
    }

    public Long getId() {
        return id;
    }

    public ArrayList<String> getTextes() {
        return textes;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getSoundUrl() {
        return soundUrl;
    }

    public ArrayList<Ask> getAsks() {
        return asks;
    }


}
