package com.englishproject;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.ColorUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Created by jean-baptistedominguez on 04/03/2016.
 */
public class BtnListener implements View.OnClickListener {

    private Button btn;
    private SequenceActivity activity;
    private int indice;

    public BtnListener(SequenceActivity sequenceActivity, Button b1, int indice) {
        this.btn = b1;
        this.activity = sequenceActivity;
        this.indice = indice;

    }

    @SuppressLint("ShowToast")
    @Override
    public void onClick(View view) {
        activity.chooseResponse(this.btn);
    }
}
