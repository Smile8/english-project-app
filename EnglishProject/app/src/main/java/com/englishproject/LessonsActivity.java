package com.englishproject;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.englishproject.adapter.LessonAdapter;
import com.englishproject.adapter.PartAdapter;
import com.englishproject.models.Lesson;

import org.w3c.dom.ls.LSException;

import java.util.ArrayList;

public class LessonsActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    private ListView lvLessons;
    private ArrayList<Lesson> lessons = new ArrayList<Lesson>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressDialog = new ProgressDialog(LessonsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading lessons");
        progressDialog.setMessage("Wait please...");
        progressDialog.show();

        lvLessons = (ListView) findViewById(R.id.lvLessons);

        lessons.add(new Lesson((long) 1, "titre", "je sais pas ce que", new ArrayList<String>()));


        LessonAdapter adapter = new LessonAdapter(LessonsActivity.this.getApplicationContext(), R.id.lvLessons, lessons);
        lvLessons.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
