package com.englishproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.englishproject.adapter.ItemMenu;
import com.englishproject.adapter.MenuAdapter;
import com.englishproject.models.User;

import java.util.ArrayList;

/**
 * Created by jean-baptistedominguez on 04/02/2016.
 */
public abstract class AppActivity extends AppCompatActivity {
    protected String URL_API = "http://api.english.showcase.lu/v1";

    private static ArrayList<ItemMenu> itemMenus = new ArrayList<ItemMenu>();

    private static User user;

    static {
        user = new User();
        itemMenus.add(new ItemMenu("Lessons", R.drawable.lessons, LessonsActivity.class));
        itemMenus.add(new ItemMenu("Credits", R.drawable.flag_green, CreditsActivity.class));
    }

    private ListView lvMenu;

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        AppActivity.user = user;
    }

    public void initMenu () {
        lvMenu = (ListView) findViewById(R.id.lvMenu);
        MenuAdapter adapter = new MenuAdapter(AppActivity.this.getApplicationContext(), R.id.lvMenu);
        lvMenu.setAdapter(adapter);
        lvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (itemMenus.get(i).getClassName() != null) {
                    Intent intent = new Intent(AppActivity.this.getApplicationContext(), (Class<?>) itemMenus.get(i).getClassName());
                    intent.putExtra("part_index", i);
                    AppActivity.this.startActivity(intent);
                }
                else {
                    Toast.makeText(AppActivity.this, "Unavailable now!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public static ArrayList<ItemMenu> getItemMenus() {
        return itemMenus;
    }

    public String getURL_API() {
        return URL_API;
    }
}
