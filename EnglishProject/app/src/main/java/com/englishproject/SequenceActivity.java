package com.englishproject;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.englishproject.adapter.PartAdapter;
import com.englishproject.adapter.ResultAdapter;
import com.englishproject.models.Ask;
import com.englishproject.models.Part;
import com.englishproject.models.Score;
import com.englishproject.models.Sequence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class SequenceActivity extends AppActivity {

    private static int TRAINING = 1;
    private static int REALISTIC = 2;

    RequestQueue requestQueue;
    private Part part;
    private ProgressDialog progressDialog;
    private ArrayList<Sequence> sequences;
    private int currentSequence = -1;
    private int currentAsk = -1;

    private ImageView imageView;
    private Button b1,b2,b3,b4;
    private TextView tTitle;
    private ImageView iwMediaRestart;
    private TextView tHeaderTitle;
    private ImageView iwCancel;

    private TextView tTimer;

    private RelativeLayout ltSuccess;
    private RelativeLayout ltMode;
    private RelativeLayout ltGame;
    private RelativeLayout ltResults;
    private Button btnTraining;
    private Button btnRealistic;

    private Score score = new Score();
    private int currentMode;
    private ListView lvResults;
    private Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sequence);

        int index = getIntent().getIntExtra("part_index", -1);

        if (index < 0) finish();
        part = MainActivity.getParts().get(index);

        progressDialog = new ProgressDialog(SequenceActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading questions");
        progressDialog.setMessage("Wait please...");
        progressDialog.show();

        // Btn pour redémarrer le son si le mode de jeu est training
        iwMediaRestart = (ImageView) findViewById(R.id.iwMediaRestart);

        // Text du timer
        tTimer = (TextView) findViewById(R.id.tTimer);

        // Image pour cancelled la game
        iwCancel = (ImageView) findViewById(R.id.iwCancel);
        iwCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopMedia();
                SequenceActivity.this.finish();
            }
        });

        // Layout pour le succès en mode training
        ltSuccess = (RelativeLayout) findViewById(R.id.ltSuccess);

        // Layout pour demander le mode de jeu
        ltMode = (RelativeLayout) findViewById(R.id.ltMode);

        // Layout pour le jeu
        ltGame = (RelativeLayout) findViewById(R.id.ltGame);

        // Layout pour les résultats
        ltResults = (RelativeLayout) findViewById(R.id.ltResults);
        lvResults = (ListView) findViewById(R.id.lvResults);

        // Btn pour retourner au menu après les résultats
        btnExit = (Button) findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // Btn pour choisir le mode training
        btnTraining = (Button) findViewById(R.id.btnTraining);
        btnTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSequence(SequenceActivity.TRAINING);
            }
        });

        // Btn pour choisir le mode réaliste
        btnRealistic = (Button) findViewById(R.id.btnRealistic);
        btnRealistic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSequence(SequenceActivity.REALISTIC);
            }
        });

        // Titre de l'entete
        tHeaderTitle = (TextView) findViewById(R.id.tHeaderTitle);
        tHeaderTitle.setText(this.part.getName());
    }

    private void restartMedia() {
        if (currentSequence >= 0
                && currentSequence < sequences.size()
                && sequences.get(currentSequence).getMediaPlayer() != null
                && sequences.get(currentSequence).getMediaPlayer().isPlaying()) {
            sequences.get(currentSequence).getMediaPlayer().pause();
            sequences.get(currentSequence).getMediaPlayer().seekTo(0);
            sequences.get(currentSequence).getMediaPlayer().start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Je stop de le media s'il est en cours
        stopMedia();
        progressDialog.dismiss();
    }

    private void stopMedia() {
        if (currentSequence >= 0
                && currentSequence < sequences.size()
                && sequences.get(currentSequence).getMediaPlayer() != null
                && sequences.get(currentSequence).getMediaPlayer().isPlaying()) {
            sequences.get(currentSequence).getMediaPlayer().stop();
            sequences.get(currentSequence).getMediaPlayer().reset();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * Requete pour récupérer les données de jeu
         */
         requestQueue = Volley.newRequestQueue(SequenceActivity.this);
         requestQueue.add(new JsonArrayRequest(getURL_API() + "/parts/" + part.getId() + "/sequences", new Response.Listener<JSONArray>() {
             @Override
             public void onResponse(JSONArray response) {
                 ArrayList<Sequence> sequences = new ArrayList<Sequence>();
                 ArrayList<String> answers;
                 ArrayList<Ask> asks;
                 ArrayList<String> res;
                 JSONObject jsonObject, jsonObject1;
                 JSONArray jsonArray, jsonArray1;
                 Ask ask;
                 Sequence seq;
                 for (int i = 0; i < response.length(); ++i) {
                     try {
                         asks = new ArrayList<Ask>();
                         jsonObject = response.getJSONObject(i);
                         jsonArray = jsonObject.getJSONArray("asks");
                         for (int j = 0; j < jsonArray.length(); ++j) {
                             jsonObject1 = jsonArray.getJSONObject(j);
                             answers = new ArrayList<String>();
                             jsonArray1 = jsonObject1.getJSONArray("answers");
                             for (int k = 0; k < jsonArray1.length(); ++k) {
                                 answers.add(jsonArray1.getString(k));
                             }
                             ask = new Ask(jsonObject1.getLong("id"), jsonObject1.getString("title"), answers, jsonObject1.getString("response"));
                             asks.add(ask);
                         }
                         seq = new Sequence(jsonObject.getLong("id"), jsonObject.getString("imageUrl"), jsonObject.getString("soundUrl"), asks);
                         sequences.add(seq);
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }
                 SequenceActivity.this.sequences = sequences;
                 askMode();
             }
         }, new Response.ErrorListener() {
             @Override
             public void onErrorResponse(VolleyError error) {
                 Toast.makeText(SequenceActivity.this.getApplicationContext(), "Echec lors de la récupération des questions", Toast.LENGTH_SHORT).show();
                 SequenceActivity.this.finish();
             }
         }));
    }

    public void askMode () {
        ltMode.setVisibility(View.VISIBLE);
        progressDialog.hide();
    }


    public void startSequence(int mode) {

        // Je sauvegarde le mode
        this.currentMode = mode;

        // On affiche le progressDialog
        progressDialog.setTitle("Loading sounds & pictures");
        progressDialog.show();

        // Init var
        this.currentSequence = -1;

        // Modifier les conditions d'utilisation des threads
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        new Thread(new Runnable() {
            @Override
            public void run() {

                // Charger tous les sons & images
                for (int i = 0; i < sequences.size(); i++) {
                    Sequence seq = sequences.get(i);
                    URL url = null;
                    if (!seq.getImageUrl().equals("null")) {
                        try {
                            url = new URL(seq.getImageUrl());
                            try {
                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                seq.setBmp(bmp);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }

                    if (!seq.getSoundUrl().equals("null")) {
                        try {
                            MediaPlayer mediaPlayer = new MediaPlayer();
                            mediaPlayer.setDataSource(seq.getSoundUrl());
                            mediaPlayer.prepare();
                            seq.setMediaPlayer(mediaPlayer);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // J'affiche le layout de la game
                        ltGame.setVisibility(View.VISIBLE);

                        // On chache la selection du mode
                        ltMode.setVisibility(View.INVISIBLE);

                        // Si c'est en training mode
                        if (isTrainingMode()) {
                            // Je rends visible le bouton pour restart le son
                            iwMediaRestart.setVisibility(View.VISIBLE);
                            iwMediaRestart.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // Je restart le son s'il en a
                                    restartMedia();
                                }
                            });
                        } else {
                            // J'affiche le timer
                            tTimer.setVisibility(View.VISIBLE);
                        }

                        // Je lance la première séquence
                        nextSequence();
                    }
                });

            }
        }).start();
    }

    private void nextSequence () {

        // Si je ne suis pas à la premiere sequence
        if (this.currentSequence >= 0) {
            // Je stop de le media s'il est en cours
            stopMedia();
        }

        // Init var
        this.currentSequence++;
        this.currentAsk = -1;

        // Si c'est la fin
        if (this.currentSequence >= this.sequences.size()) {

            // Je sauvegarde le score si c'est en mode réaliste
            if (!isTrainingMode()) {
                getUser().saveScore(part, score);

                if (getUser().getFacebookId() != null) {

                    JSONObject params = new JSONObject();
                    JSONObject partObject = new JSONObject();
                    try {
                        partObject.put("id", this.part.getId());
                        params.put("score", score.getPoints());
                        params.put("facebookId", getUser().getFacebookId());
                        params.put("part", partObject);
                        requestQueue.add(new JsonObjectRequest(Request.Method.POST, getURL_API() + "/scores", params, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Toast.makeText(SequenceActivity.this, "Score saved for the facebook user " + getUser().getFacebookId(), Toast.LENGTH_SHORT).show();
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(SequenceActivity.this, "Error when score saved for the facebook user " + getUser().getFacebookId(), Toast.LENGTH_SHORT).show();
                            }
                        }));
                    } catch (JSONException e) {
                        Toast.makeText(SequenceActivity.this, "Error when score saved for the facebook user " + getUser().getFacebookId(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }

                //
                // Je regarde les scores
                //

                // Je mets le loading
                this.progressDialog.setTitle("Loading scores");
                this.progressDialog.setMessage("Wait please...");
                this.progressDialog.show();
                this.ltGame.setVisibility(View.INVISIBLE);
                this.ltResults.setVisibility(View.VISIBLE);

                // Je charge les résultats
                ResultAdapter adapter = new ResultAdapter(SequenceActivity.this, R.id.lvResults, sequences);
                lvResults.setAdapter(adapter);

                // Je cache le dialog
                this.progressDialog.hide();
            }
            else {
                // Je ferme
                this.finish();
            }


            // Je m'arrete la dans la fonction
            return;
        }

        // Je charge l'image de la prochaine séquence
        imageView = (ImageView) findViewById(R.id.imageView);
        if (sequences.get(SequenceActivity.this.currentSequence).getImageUrl().equals("null")) {
            imageView.setImageResource(R.drawable.no_picture);
        }
        else {
            imageView.setImageBitmap(sequences.get(SequenceActivity.this.currentSequence).getBmp());
        }

        // Je lance la première question
        nextAsk();
    }

    private void nextAsk () {

        // Init var
        this.currentAsk++;

        // Si c'est la fin des questions
        if (this.currentAsk >= this.sequences.get(this.currentSequence).getAsks().size()) {
            // Je passe à la séquence suivante!
            this.nextSequence();

            // Je m'arrete dans la fonction
            return;
        }

        // Je cache le dialogue, ça commence!
        progressDialog.hide();

        //
        // Je charge les données de la question dans le layout
        //

        this.tTitle = (TextView) findViewById(R.id.tTitle);
        this.tTitle.setText("Question " + (this.currentSequence + 1) + "." + (this.currentAsk + 1));

        this.b1 = (Button) findViewById(R.id.btn1);
        this.b1.setBackgroundColor(Color.parseColor("#FFEEE855"));
        this.b1.setText(this.sequences.get(this.currentSequence).getAsks().get(currentAsk).getAnswers().get(0));
        this.b1.setOnClickListener(new BtnListener(SequenceActivity.this, this.b1, 0));

        this.b2 = (Button) findViewById(R.id.btn2);
        this.b2.setBackgroundColor(Color.parseColor("#FFEEE855"));
        this.b2.setText(this.sequences.get(this.currentSequence).getAsks().get(currentAsk).getAnswers().get(1));
        this.b2.setOnClickListener(new BtnListener(SequenceActivity.this, this.b2, 1));

        this.b3 = (Button) findViewById(R.id.btn3);
        this.b3.setBackgroundColor(Color.parseColor("#FFEEE855"));
        this.b3.setText(this.sequences.get(this.currentSequence).getAsks().get(currentAsk).getAnswers().get(2));
        this.b3.setOnClickListener(new BtnListener(SequenceActivity.this, this.b3, 2));

        this.b4 = (Button) findViewById(R.id.btn4);
        if (this.sequences.get(this.currentSequence).getAsks().get(currentAsk).getAnswers().size()>3) {
            this.b4.setBackgroundColor(Color.parseColor("#FFEEE855"));
            this.b4.setText(this.sequences.get(this.currentSequence).getAsks().get(currentAsk).getAnswers().get(3));
            this.b4.setOnClickListener(new BtnListener(SequenceActivity.this, this.b4, 3));
        }
        else {
            this.b4.setVisibility(View.INVISIBLE);
        }


        /**
         * Lancement du compte à rebourd pour le début de la question
         */

        // Je démarre le son si c'est la premiere question de la séquence
        if (SequenceActivity.this.currentAsk == 0) {
            final int timer = 3;
            progressDialog.setTitle("Question " + (currentSequence + 1) + "." + (currentAsk + 1) + "-" + (currentAsk+sequences.get(currentSequence).getAsks().size()));
            progressDialog.setMessage("in " + timer);
            progressDialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    for (int i = 0; i < timer; i++) {
                        final int finalI = i;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.setMessage("in " + (timer - finalI));
                            }
                        });
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (i + 1 == timer) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.hide();
                                    /*Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(SequenceActivity.this, R.animator.hyperspace_jump);
                                    ltGame.startAnimation(hyperspaceJumpAnimation);*/

                                    // Je démarre le son si c'est la premiere question de la séquence
                                    if (sequences.get(SequenceActivity.this.currentSequence).getMediaPlayer() != null) {
                                        sequences.get(SequenceActivity.this.currentSequence).getMediaPlayer().start();
                                    }
                                }
                            });
                        }
                    }
                }
            }).start();
        }


        //
        // Fin du chargement des données
        //
    }

    public RelativeLayout getLtSuccess() {
        return ltSuccess;
    }

    public boolean isTrainingMode() {
        return currentMode == SequenceActivity.TRAINING;
    }

    public void chooseResponse(Button btn) {
        String response = sequences.get(currentSequence).getAsks().get(currentAsk).getResponse();
        score.addResponse(sequences.get(currentSequence).getAsks().get(currentAsk), btn.getText().charAt(0) + "");

        if (isTrainingMode())
            if (response.equals(btn.getText())) {
                btn.setBackgroundColor(Color.parseColor("#FF7FBE3E"));


                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getLtSuccess().setVisibility(View.VISIBLE);
                            }
                        });

                        try {
                            Thread.sleep(1500);
                        } catch (InterruptedException e) {}

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getLtSuccess().setVisibility(View.INVISIBLE);
                                nextAsk();
                            }
                        });
                    }
                }).start();
            }

            else

            {
                btn.setBackgroundColor(Color.parseColor("#FFBE4B40"));
            }
        else {
            if (response.equals(btn.getText())) {
                score.addPoints(10);
            }
            nextAsk();
        }
    }

    public Score getScore() {
        return score;
    }
}
