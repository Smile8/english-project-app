package com.englishproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.englishproject.adapter.MenuAdapter;
import com.englishproject.adapter.PartAdapter;
import com.englishproject.models.Part;
import com.englishproject.models.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppActivity {

    RequestQueue requestQueue;
    private static ArrayList<Part> parts = new ArrayList<Part>();
    ListView lvParts;
    MediaPlayer mediaPlayer;
    private ImageView iwMediaStart;
    private ImageView iwMediaPause;
    private ImageView iwMenu;
    private DrawerLayout drawerLayout;
    private CallbackManager callbackManager;
    private TextView tFirstname;
    private TextView tLastname;
    private LoginButton btnLoginFacebook;
    private ProfileTracker profileTracker;
    private ImageView iwProfile;

    public static ArrayList<Part> getParts() {
        return parts;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        setContentView(R.layout.activity_main);

        initMenu();

        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource("http://docs.english.showcase.lu/audios/intro.mp3");
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        iwMediaStart = (ImageView) findViewById(R.id.iwMediaStart);
        iwMediaPause = (ImageView) findViewById(R.id.iwMediaPause);
        iwMenu = (ImageView) findViewById(R.id.iwMenu);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawLayout);
        tFirstname = (TextView) findViewById(R.id.tFirstname);
        tLastname = (TextView) findViewById(R.id.tLastname);
        iwProfile = (ImageView) findViewById(R.id.iwProfile);

        iwMediaStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.start();
                iwMediaPause.setVisibility(View.VISIBLE);
                iwMediaStart.setVisibility(View.INVISIBLE);
            }
        });

        iwMediaPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.pause();
                iwMediaStart.setVisibility(View.VISIBLE);
                iwMediaPause.setVisibility(View.INVISIBLE);
            }
        });

        iwMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        lvParts = (ListView) findViewById(R.id.lvParts);
        lvParts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Part part = (Part) lvParts.getItemAtPosition(i);
                Toast.makeText(MainActivity.this.getApplicationContext(), "Good luck!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this.getApplicationContext(), SequenceActivity.class);
                intent.putExtra("part_index", i);
                MainActivity.this.startActivity(intent);
            }
        });

        if (parts.size()==0) {
            requestQueue = Volley.newRequestQueue(MainActivity.this.getApplicationContext());
            requestQueue.add(new JsonArrayRequest(getURL_API() + "/parts", new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    parts.clear();
                    JSONObject partObject;
                    for (int i = 0; i < response.length(); ++i) {
                        try {
                            partObject = response.getJSONObject(i);
                            parts.add(new Part(partObject.getLong("id"), partObject.getString("name"), partObject.getInt("highScore")));
                        } catch (JSONException e) {
                            Log.d("LOG_PART_ERROR", "Erreur lors de la récupération de l'élément " + i);
                        }
                    }
                    MainActivity.this.parts = parts;
                    PartAdapter adapter = new PartAdapter(MainActivity.this.getApplicationContext(), R.id.lvParts, parts);
                    lvParts.setAdapter(adapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("LOG_PARTS_REQUEST", "Erreur lors de la requete" + error.toString());
                    finish();
                }
            }));
        }


        callbackManager = CallbackManager.Factory.create();
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                // App code
                if (currentProfile != null) {
                    getUser().setFirstname(currentProfile.getFirstName());
                    getUser().setLastname(currentProfile.getLastName());
                    getUser().setFacebookId(currentProfile.getId());

                    // Modifier les conditions d'utilisation des threads
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);

                    try {
                        URL url = new URL(currentProfile.getProfilePictureUri(300, 300).toString());
                        try {
                            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            iwProfile.setImageBitmap(bmp);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                    Toast.makeText(getApplicationContext(), "Salut " + getUser().getFirstname() + " " + getUser().getLastname(), Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Au revoir " + getUser().getFirstname() + " " + getUser().getLastname(), Toast.LENGTH_SHORT).show();
                    setUser(new User());
                }
                tFirstname.setText(getUser().getFirstname());
                tLastname.setText(getUser().getLastname());
                iwProfile.setImageResource(R.drawable.profile);
            }
        };

        btnLoginFacebook = (LoginButton) findViewById(R.id.login_button);
        btnLoginFacebook.setReadPermissions("public_profile");
        btnLoginFacebook.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        AccessToken accessToken = loginResult.getAccessToken();
                        Profile p = Profile.getCurrentProfile();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
        //Intent intent = new Intent(MainActivity.this.getApplicationContext(), LessonsActivity.class);
        //intent.putExtra("lesson_index", i);
        //LessonsActivity.this.startActivity(intent);

        Profile p = Profile.getCurrentProfile();
        if (p != null) {
            Toast.makeText(getApplicationContext(), "Salut " + p.getFirstName() + " " + p.getLastName(), Toast.LENGTH_SHORT).show();
            getUser().setFirstname(p.getFirstName());
            getUser().setLastname(p.getLastName());
            getUser().setFacebookId(p.getId());
            tFirstname.setText(getUser().getFirstname());
            tLastname.setText(getUser().getLastname());

            // Modifier les conditions d'utilisation des threads
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try {
                URL url = new URL(p.getProfilePictureUri(300, 300).toString());
                try {
                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    iwProfile.setImageBitmap(bmp);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        profileTracker.stopTracking();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PartAdapter adapter = new PartAdapter(MainActivity.this.getApplicationContext(), R.id.lvParts, parts);
        lvParts.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }
}
