package com.englishproject.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.englishproject.R;
import com.englishproject.SequenceActivity;
import com.englishproject.models.Ask;
import com.englishproject.models.Sequence;

import java.util.ArrayList;

/**
 * Created by jean-baptistedominguez on 04/02/2016.
 */
public class ResultAskAdapter extends ArrayAdapter<Ask> {

    SequenceActivity activity;
    Sequence sequence;
    int positionSequence;

    public ResultAskAdapter(SequenceActivity activity, int resource, ArrayList<Ask> asks, Sequence sequence, int positionSequence) {
        super(activity.getApplicationContext(), resource, asks);
        this.activity = activity;
        this.sequence = sequence;
        this.positionSequence = positionSequence;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Ask item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_item_result_ask, parent, false);
        }
        TextView tName = (TextView) convertView.findViewById(R.id.tQuestion);
        tName.setText("Q." + (positionSequence+1) + "." + (sequence.getAsks().indexOf(item)+1));
        ImageView iwIcon = (ImageView) convertView.findViewById(R.id.iwState);
        if (activity.getScore().getResponseByAsk(item).equals(item.getResponse())) {
            iwIcon.setImageResource(R.drawable.response_true);
        }
        else {
            iwIcon.setImageResource(R.drawable.response_false);
        }
        return convertView;
    }

}
