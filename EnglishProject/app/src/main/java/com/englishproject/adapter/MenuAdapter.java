package com.englishproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.englishproject.AppActivity;
import com.englishproject.R;

/**
 * Created by jean-baptistedominguez on 04/02/2016.
 */
public class MenuAdapter extends ArrayAdapter<ItemMenu> {

    public MenuAdapter(Context context, int resource) {
        super(context, resource, AppActivity.getItemMenus());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemMenu item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_item_menu, parent, false);
        }
        TextView tName = (TextView) convertView.findViewById(R.id.tQuestion);
        tName.setText(item.getName());
        ImageView iwIcon = (ImageView) convertView.findViewById(R.id.iwIcon);
        iwIcon.setImageResource(item.getDrawable());
        return convertView;
    }

}
