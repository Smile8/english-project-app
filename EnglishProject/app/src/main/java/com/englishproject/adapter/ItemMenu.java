package com.englishproject.adapter;

import android.graphics.drawable.Drawable;

import com.englishproject.CreditsActivity;

/**
 * Created by jean-baptistedominguez on 12/03/2016.
 */
public class ItemMenu {


    private final String name;
    private final int drawable;
    private final Object className;

    public ItemMenu(String name, int drawable, Class className) {
        this.name = name;
        this.drawable = drawable;
        this.className = className;
    }

    public Object getClassName() {
        return className;
    }

    public String getName() {
        return name;
    }

    public int getDrawable() {
        return drawable;
    }
}
