package com.englishproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.englishproject.AppActivity;
import com.englishproject.R;
import com.englishproject.models.Lesson;

import java.util.ArrayList;

/**
 * Created by panpriape on 15/03/16.
 */
public class LessonAdapter extends ArrayAdapter<Lesson> {

    public LessonAdapter(Context context, int resource, ArrayList<Lesson> lessons) {
        super(context, resource, lessons);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Lesson item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_item_lesson, parent, false);
        }
        TextView tName = (TextView) convertView.findViewById(R.id.tTitle);
        tName.setText(item.getTitle());

        return convertView;
    }
}