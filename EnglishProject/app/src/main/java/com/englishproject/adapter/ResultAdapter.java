package com.englishproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.englishproject.R;
import com.englishproject.SequenceActivity;
import com.englishproject.models.Sequence;

import java.util.ArrayList;

/**
 * Created by jean-baptistedominguez on 04/02/2016.
 */
public class ResultAdapter extends ArrayAdapter<Sequence> {

    SequenceActivity activity;

    public ResultAdapter(SequenceActivity activity, int resource, ArrayList<Sequence> sequences) {
        super(activity.getApplicationContext(), resource, sequences);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Sequence item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_item_result, parent, false);
        }
        ListView lvResultAsks = (ListView) convertView.findViewById(R.id.lvResultAsks);
        ResultAskAdapter adapter = new ResultAskAdapter(activity, R.id.lvResultAsks, item.getAsks(), item, position);
        lvResultAsks.setAdapter(adapter);

        return convertView;
    }

}
