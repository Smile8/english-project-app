package com.englishproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.englishproject.AppActivity;
import com.englishproject.R;
import com.englishproject.models.Part;
import com.englishproject.models.Score;

import java.util.ArrayList;

/**
 * Created by jean-baptistedominguez on 04/02/2016.
 */
public class PartAdapter extends ArrayAdapter<Part> {

    public PartAdapter(Context context, int resource, ArrayList<Part> parts) {
        super(context, resource, parts);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Part part = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_item_part, parent, false);
        }
        TextView tName = (TextView) convertView.findViewById(R.id.tQuestion);
        tName.setText(part.getName());

        ProgressBar pbScore = (ProgressBar) convertView.findViewById(R.id.pbScore);
        TextView tScore = (TextView) convertView.findViewById(R.id.tScore);
        Score score;
        if ((score = AppActivity.getUser().getHighScore(part)) != null) {
            tScore.setText(score.getPoints() + " pts");
            if (part.getHighScore()>0) {
                pbScore.setProgress((score.getPoints()*100)/part.getHighScore());
            } else {
                pbScore.setProgress(0);
            }
        }
        else {
            tScore.setText("no score");
            pbScore.setProgress(0);
        }
        return convertView;
    }
}
